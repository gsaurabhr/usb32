<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.1">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="NINA-W102">
<description>&lt;STAND-ALONE MULTIRADIO MODULE&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="NINAW102">
<description>&lt;b&gt;NINA-W102&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-6.2" y="-3.875" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="-5.2" y="-3.875" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="-4.2" y="-3.875" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="4" x="-3.2" y="-3.875" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="5" x="-2.2" y="-3.875" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="6" x="-1.2" y="-3.875" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="7" x="-0.2" y="-3.875" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="8" x="0.8" y="-3.875" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="9" x="1.8" y="-3.875" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="10" x="2.8" y="-3.875" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="11" x="2.725" y="-1.75" dx="1.15" dy="0.7" layer="1"/>
<smd name="12" x="2.725" y="-0.75" dx="1.15" dy="0.7" layer="1"/>
<smd name="13" x="2.725" y="0.25" dx="1.15" dy="0.7" layer="1"/>
<smd name="14" x="2.725" y="1.25" dx="1.15" dy="0.7" layer="1"/>
<smd name="15" x="2.725" y="2.25" dx="1.15" dy="0.7" layer="1"/>
<smd name="16" x="2.8" y="4.375" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="17" x="1.8" y="4.375" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="18" x="0.8" y="4.375" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="19" x="-0.2" y="4.375" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="20" x="-1.2" y="4.375" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="21" x="-2.2" y="4.375" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="22" x="-3.2" y="4.375" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="23" x="-4.2" y="4.375" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="24" x="-5.2" y="4.375" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="25" x="-6.2" y="4.375" dx="1.15" dy="0.7" layer="1" rot="R90"/>
<smd name="26" x="-6.125" y="2.25" dx="1.15" dy="0.7" layer="1"/>
<smd name="27" x="-6.125" y="1.25" dx="1.15" dy="0.7" layer="1"/>
<smd name="28" x="-6.125" y="0.25" dx="1.15" dy="0.7" layer="1"/>
<smd name="29" x="-6.125" y="-0.75" dx="1.15" dy="0.7" layer="1"/>
<smd name="30" x="-6.125" y="-1.75" dx="1.15" dy="0.7" layer="1"/>
<smd name="31" x="-4.75" y="-2.5" dx="0.7" dy="0.7" layer="1"/>
<smd name="32" x="-4.75" y="-1.4" dx="0.7" dy="0.7" layer="1"/>
<smd name="33" x="-4.75" y="-0.3" dx="0.7" dy="0.7" layer="1"/>
<smd name="34" x="-4.75" y="0.8" dx="0.7" dy="0.7" layer="1"/>
<smd name="35" x="-4.75" y="1.9" dx="0.7" dy="0.7" layer="1"/>
<smd name="36" x="-4.75" y="3" dx="0.7" dy="0.7" layer="1"/>
<smd name="37" x="-3.42" y="-0.325" dx="0.7" dy="0.7" layer="1"/>
<smd name="38" x="-3.42" y="0.825" dx="0.7" dy="0.7" layer="1"/>
<smd name="39" x="-2.27" y="-1.475" dx="0.7" dy="0.7" layer="1"/>
<smd name="40" x="-2.27" y="-0.325" dx="0.7" dy="0.7" layer="1"/>
<smd name="41" x="-2.27" y="0.825" dx="0.7" dy="0.7" layer="1"/>
<smd name="42" x="-2.27" y="1.975" dx="0.7" dy="0.7" layer="1"/>
<smd name="43" x="-1.12" y="-0.325" dx="0.7" dy="0.7" layer="1"/>
<smd name="44" x="-1.12" y="0.825" dx="0.7" dy="0.7" layer="1"/>
<smd name="45" x="0.03" y="-1.475" dx="0.7" dy="0.7" layer="1"/>
<smd name="46" x="0.03" y="-0.325" dx="0.7" dy="0.7" layer="1"/>
<smd name="47" x="0.03" y="0.825" dx="0.7" dy="0.7" layer="1"/>
<smd name="48" x="0.03" y="1.975" dx="0.7" dy="0.7" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-7" y1="5.25" x2="7" y2="5.25" width="0.2" layer="51"/>
<wire x1="7" y1="5.25" x2="7" y2="-4.75" width="0.2" layer="51"/>
<wire x1="7" y1="-4.75" x2="-7" y2="-4.75" width="0.2" layer="51"/>
<wire x1="-7" y1="-4.75" x2="-7" y2="5.25" width="0.2" layer="51"/>
<wire x1="-7" y1="5.25" x2="7" y2="5.25" width="0.1" layer="21"/>
<wire x1="7" y1="5.25" x2="7" y2="-4.75" width="0.1" layer="21"/>
<wire x1="7" y1="-4.75" x2="-7" y2="-4.75" width="0.1" layer="21"/>
<wire x1="-7" y1="-4.75" x2="-7" y2="5.25" width="0.1" layer="21"/>
<wire x1="-8" y1="6.25" x2="8" y2="6.25" width="0.1" layer="51"/>
<wire x1="8" y1="6.25" x2="8" y2="-6.25" width="0.1" layer="51"/>
<wire x1="8" y1="-6.25" x2="-8" y2="-6.25" width="0.1" layer="51"/>
<wire x1="-8" y1="-6.25" x2="-8" y2="6.25" width="0.1" layer="51"/>
<wire x1="-6.3" y1="-5.15" x2="-6.3" y2="-5.15" width="0.2" layer="21"/>
<wire x1="-6.3" y1="-5.15" x2="-6.1" y2="-5.15" width="0.2" layer="21" curve="-180"/>
<wire x1="-6.1" y1="-5.15" x2="-6.1" y2="-5.15" width="0.2" layer="21"/>
<wire x1="-6.1" y1="-5.15" x2="-6.3" y2="-5.15" width="0.2" layer="21" curve="-180"/>
</package>
</packages>
<symbols>
<symbol name="NINA-W102">
<wire x1="5.08" y1="2.54" x2="96.52" y2="2.54" width="0.254" layer="94"/>
<wire x1="96.52" y1="-60.96" x2="96.52" y2="2.54" width="0.254" layer="94"/>
<wire x1="96.52" y1="-60.96" x2="5.08" y2="-60.96" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-60.96" width="0.254" layer="94"/>
<text x="97.79" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="97.79" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="GPIO_1/_SPI_V_DI" x="0" y="0" length="middle"/>
<pin name="GPI_2/_ADC_2" x="0" y="-2.54" length="middle"/>
<pin name="GPI_3/_ADC_3" x="0" y="-5.08" length="middle"/>
<pin name="GPI_4/_ADC_4" x="0" y="-7.62" length="middle"/>
<pin name="GPIO_5/_LPO_IN" x="0" y="-10.16" length="middle"/>
<pin name="GND_1" x="0" y="-12.7" length="middle"/>
<pin name="GPIO_7" x="0" y="-15.24" length="middle"/>
<pin name="GPIO_8/_RMII_TXEN/_SPI_V_HD/" x="0" y="-17.78" length="middle"/>
<pin name="VCC_IO" x="0" y="-20.32" length="middle"/>
<pin name="VCC" x="0" y="-22.86" length="middle"/>
<pin name="RSVD_1" x="0" y="-25.4" length="middle"/>
<pin name="GND_2" x="0" y="-27.94" length="middle"/>
<pin name="ANT" x="0" y="-30.48" length="middle"/>
<pin name="GND_3" x="0" y="-33.02" length="middle"/>
<pin name="RSVD_2" x="0" y="-35.56" length="middle"/>
<pin name="GPIO_16/_RMII_RXD0/_DAC_16" x="0" y="-38.1" length="middle"/>
<pin name="GPIO_17/_RMII_RXD1/_DAC_17" x="0" y="-40.64" length="middle"/>
<pin name="GPIO_18/_RMII_CRSDV" x="0" y="-43.18" length="middle"/>
<pin name="RESET_N" x="0" y="-45.72" length="middle"/>
<pin name="GPIO_20/_UART_RTS/_RMII_TXD1/_SPI_V_WP" x="0" y="-48.26" length="middle"/>
<pin name="GPIO_21/_UART_CTS/_RMII_TXD0/_SPI_V_DO/" x="0" y="-50.8" length="middle"/>
<pin name="GPIO_22/_UART_TXD" x="0" y="-53.34" length="middle"/>
<pin name="GPIO_23/_UART_RXD" x="0" y="-55.88" length="middle"/>
<pin name="GPIO_24/_RMII_MDIO" x="0" y="-58.42" length="middle"/>
<pin name="GPIO_25/_RMII_MDCLK/" x="101.6" y="0" length="middle" rot="R180"/>
<pin name="GND_4" x="101.6" y="-2.54" length="middle" rot="R180"/>
<pin name="GPIO_27/_RMII_CLK/_SYS_BOOT" x="101.6" y="-5.08" length="middle" rot="R180"/>
<pin name="GPIO_28/_SPI_V_CS" x="101.6" y="-7.62" length="middle" rot="R180"/>
<pin name="GPIO_29/_SPI_V_CLK" x="101.6" y="-10.16" length="middle" rot="R180"/>
<pin name="GND_5" x="101.6" y="-12.7" length="middle" rot="R180"/>
<pin name="GPIO_31/_JTAG_TMS" x="101.6" y="-15.24" length="middle" rot="R180"/>
<pin name="GPIO_32/_JTAG_TDO" x="101.6" y="-17.78" length="middle" rot="R180"/>
<pin name="RSVD_3" x="101.6" y="-20.32" length="middle" rot="R180"/>
<pin name="GPI_34/_ADC_3" x="101.6" y="-22.86" length="middle" rot="R180"/>
<pin name="GPIO_35/_JTAG_CLK" x="101.6" y="-25.4" length="middle" rot="R180"/>
<pin name="GPIO_36/_JTAG_TDI/" x="101.6" y="-27.94" length="middle" rot="R180"/>
<pin name="GND_6" x="101.6" y="-30.48" length="middle" rot="R180"/>
<pin name="GND_7" x="101.6" y="-33.02" length="middle" rot="R180"/>
<pin name="GND_8" x="101.6" y="-35.56" length="middle" rot="R180"/>
<pin name="GND_9" x="101.6" y="-38.1" length="middle" rot="R180"/>
<pin name="GND_10" x="101.6" y="-40.64" length="middle" rot="R180"/>
<pin name="GND_11" x="101.6" y="-43.18" length="middle" rot="R180"/>
<pin name="GND_12" x="101.6" y="-45.72" length="middle" rot="R180"/>
<pin name="GND_13" x="101.6" y="-48.26" length="middle" rot="R180"/>
<pin name="GND_14" x="101.6" y="-50.8" length="middle" rot="R180"/>
<pin name="GND_15" x="101.6" y="-53.34" length="middle" rot="R180"/>
<pin name="GND_16" x="101.6" y="-55.88" length="middle" rot="R180"/>
<pin name="GND_17" x="101.6" y="-58.42" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="NINA-W102" prefix="IC">
<description>&lt;b&gt;STAND-ALONE MULTIRADIO MODULE&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.u-blox.com/sites/default/files/NINA-W10_DataSheet_(UBX-17065507).pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="NINA-W102" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NINAW102">
<connects>
<connect gate="G$1" pin="ANT" pad="13"/>
<connect gate="G$1" pin="GND_1" pad="6"/>
<connect gate="G$1" pin="GND_10" pad="41"/>
<connect gate="G$1" pin="GND_11" pad="42"/>
<connect gate="G$1" pin="GND_12" pad="43"/>
<connect gate="G$1" pin="GND_13" pad="44"/>
<connect gate="G$1" pin="GND_14" pad="45"/>
<connect gate="G$1" pin="GND_15" pad="46"/>
<connect gate="G$1" pin="GND_16" pad="47"/>
<connect gate="G$1" pin="GND_17" pad="48"/>
<connect gate="G$1" pin="GND_2" pad="12"/>
<connect gate="G$1" pin="GND_3" pad="14"/>
<connect gate="G$1" pin="GND_4" pad="26"/>
<connect gate="G$1" pin="GND_5" pad="30"/>
<connect gate="G$1" pin="GND_6" pad="37"/>
<connect gate="G$1" pin="GND_7" pad="38"/>
<connect gate="G$1" pin="GND_8" pad="39"/>
<connect gate="G$1" pin="GND_9" pad="40"/>
<connect gate="G$1" pin="GPIO_1/_SPI_V_DI" pad="1"/>
<connect gate="G$1" pin="GPIO_16/_RMII_RXD0/_DAC_16" pad="16"/>
<connect gate="G$1" pin="GPIO_17/_RMII_RXD1/_DAC_17" pad="17"/>
<connect gate="G$1" pin="GPIO_18/_RMII_CRSDV" pad="18"/>
<connect gate="G$1" pin="GPIO_20/_UART_RTS/_RMII_TXD1/_SPI_V_WP" pad="20"/>
<connect gate="G$1" pin="GPIO_21/_UART_CTS/_RMII_TXD0/_SPI_V_DO/" pad="21"/>
<connect gate="G$1" pin="GPIO_22/_UART_TXD" pad="22"/>
<connect gate="G$1" pin="GPIO_23/_UART_RXD" pad="23"/>
<connect gate="G$1" pin="GPIO_24/_RMII_MDIO" pad="24"/>
<connect gate="G$1" pin="GPIO_25/_RMII_MDCLK/" pad="25"/>
<connect gate="G$1" pin="GPIO_27/_RMII_CLK/_SYS_BOOT" pad="27"/>
<connect gate="G$1" pin="GPIO_28/_SPI_V_CS" pad="28"/>
<connect gate="G$1" pin="GPIO_29/_SPI_V_CLK" pad="29"/>
<connect gate="G$1" pin="GPIO_31/_JTAG_TMS" pad="31"/>
<connect gate="G$1" pin="GPIO_32/_JTAG_TDO" pad="32"/>
<connect gate="G$1" pin="GPIO_35/_JTAG_CLK" pad="35"/>
<connect gate="G$1" pin="GPIO_36/_JTAG_TDI/" pad="36"/>
<connect gate="G$1" pin="GPIO_5/_LPO_IN" pad="5"/>
<connect gate="G$1" pin="GPIO_7" pad="7"/>
<connect gate="G$1" pin="GPIO_8/_RMII_TXEN/_SPI_V_HD/" pad="8"/>
<connect gate="G$1" pin="GPI_2/_ADC_2" pad="2"/>
<connect gate="G$1" pin="GPI_3/_ADC_3" pad="3"/>
<connect gate="G$1" pin="GPI_34/_ADC_3" pad="34"/>
<connect gate="G$1" pin="GPI_4/_ADC_4" pad="4"/>
<connect gate="G$1" pin="RESET_N" pad="19"/>
<connect gate="G$1" pin="RSVD_1" pad="11"/>
<connect gate="G$1" pin="RSVD_2" pad="15"/>
<connect gate="G$1" pin="RSVD_3" pad="33"/>
<connect gate="G$1" pin="VCC" pad="10"/>
<connect gate="G$1" pin="VCC_IO" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="STAND-ALONE MULTIRADIO MODULE" constant="no"/>
<attribute name="HEIGHT" value="3mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="U-Blox" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="NINA-W102" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="USB2244-AEZG-06">
<description>&lt;USB 2.0 SD/MMC Flash Controller QFN36 Microchip USB2244-AEZG-06, USB Controller, 35MBps, USB 2.0, 3.3 V, 36-Pin QFN&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="QFN50P600X600X90-37N">
<description>&lt;b&gt;36-Pin QFN_1&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.85" y="2" dx="1.05" dy="0.3" layer="1"/>
<smd name="2" x="-2.85" y="1.5" dx="1.05" dy="0.3" layer="1"/>
<smd name="3" x="-2.85" y="1" dx="1.05" dy="0.3" layer="1"/>
<smd name="4" x="-2.85" y="0.5" dx="1.05" dy="0.3" layer="1"/>
<smd name="5" x="-2.85" y="0" dx="1.05" dy="0.3" layer="1"/>
<smd name="6" x="-2.85" y="-0.5" dx="1.05" dy="0.3" layer="1"/>
<smd name="7" x="-2.85" y="-1" dx="1.05" dy="0.3" layer="1"/>
<smd name="8" x="-2.85" y="-1.5" dx="1.05" dy="0.3" layer="1"/>
<smd name="9" x="-2.85" y="-2" dx="1.05" dy="0.3" layer="1"/>
<smd name="10" x="-2" y="-2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="-1.5" y="-2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="-1" y="-2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="-0.5" y="-2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="0" y="-2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="0.5" y="-2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="1" y="-2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="1.5" y="-2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="18" x="2" y="-2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="19" x="2.85" y="-2" dx="1.05" dy="0.3" layer="1"/>
<smd name="20" x="2.85" y="-1.5" dx="1.05" dy="0.3" layer="1"/>
<smd name="21" x="2.85" y="-1" dx="1.05" dy="0.3" layer="1"/>
<smd name="22" x="2.85" y="-0.5" dx="1.05" dy="0.3" layer="1"/>
<smd name="23" x="2.85" y="0" dx="1.05" dy="0.3" layer="1"/>
<smd name="24" x="2.85" y="0.5" dx="1.05" dy="0.3" layer="1"/>
<smd name="25" x="2.85" y="1" dx="1.05" dy="0.3" layer="1"/>
<smd name="26" x="2.85" y="1.5" dx="1.05" dy="0.3" layer="1"/>
<smd name="27" x="2.85" y="2" dx="1.05" dy="0.3" layer="1"/>
<smd name="28" x="2" y="2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="29" x="1.5" y="2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="30" x="1" y="2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="31" x="0.5" y="2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="32" x="0" y="2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="33" x="-0.5" y="2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="34" x="-1" y="2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="35" x="-1.5" y="2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="36" x="-2" y="2.85" dx="1.05" dy="0.3" layer="1" rot="R90"/>
<smd name="37" x="0" y="0" dx="4.2" dy="4.2" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.6" y1="3.6" x2="3.6" y2="3.6" width="0.05" layer="51"/>
<wire x1="3.6" y1="3.6" x2="3.6" y2="-3.6" width="0.05" layer="51"/>
<wire x1="3.6" y1="-3.6" x2="-3.6" y2="-3.6" width="0.05" layer="51"/>
<wire x1="-3.6" y1="-3.6" x2="-3.6" y2="3.6" width="0.05" layer="51"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.1" layer="51"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.1" layer="51"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.1" layer="51"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.1" layer="51"/>
<wire x1="-3" y1="2.5" x2="-2.5" y2="3" width="0.1" layer="51"/>
<circle x="-3.375" y="2.75" radius="0.125" width="0.25" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="USB2244-AEZG-06">
<wire x1="5.08" y1="25.4" x2="33.02" y2="25.4" width="0.254" layer="94"/>
<wire x1="33.02" y1="-35.56" x2="33.02" y2="25.4" width="0.254" layer="94"/>
<wire x1="33.02" y1="-35.56" x2="5.08" y2="-35.56" width="0.254" layer="94"/>
<wire x1="5.08" y1="25.4" x2="5.08" y2="-35.56" width="0.254" layer="94"/>
<text x="34.29" y="30.48" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="34.29" y="27.94" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="LED" x="0" y="0" length="middle"/>
<pin name="USB+" x="0" y="-2.54" length="middle"/>
<pin name="USB-" x="0" y="-5.08" length="middle"/>
<pin name="SD_D1" x="0" y="-7.62" length="middle"/>
<pin name="SD_D0" x="0" y="-10.16" length="middle"/>
<pin name="VDD33_1" x="0" y="-12.7" length="middle"/>
<pin name="SD_D7" x="0" y="-15.24" length="middle"/>
<pin name="SD_D6" x="0" y="-17.78" length="middle"/>
<pin name="SD_CLK" x="0" y="-20.32" length="middle"/>
<pin name="SD_D5" x="7.62" y="-40.64" length="middle" rot="R90"/>
<pin name="SD_CMD" x="10.16" y="-40.64" length="middle" rot="R90"/>
<pin name="NC_1" x="12.7" y="-40.64" length="middle" rot="R90"/>
<pin name="VDD18" x="15.24" y="-40.64" length="middle" rot="R90"/>
<pin name="VDD33_2" x="17.78" y="-40.64" length="middle" rot="R90"/>
<pin name="NC_2" x="20.32" y="-40.64" length="middle" rot="R90"/>
<pin name="NC_3" x="22.86" y="-40.64" length="middle" rot="R90"/>
<pin name="NC_4" x="25.4" y="-40.64" length="middle" rot="R90"/>
<pin name="RESET_N" x="27.94" y="-40.64" length="middle" rot="R90"/>
<pin name="RXD/SDA" x="38.1" y="0" length="middle" rot="R180"/>
<pin name="SD_NCD" x="38.1" y="-2.54" length="middle" rot="R180"/>
<pin name="SD_D2" x="38.1" y="-5.08" length="middle" rot="R180"/>
<pin name="NC_6" x="38.1" y="-7.62" length="middle" rot="R180"/>
<pin name="SD_D3" x="38.1" y="-10.16" length="middle" rot="R180"/>
<pin name="VDD33_3" x="38.1" y="-12.7" length="middle" rot="R180"/>
<pin name="CRD_PWR" x="38.1" y="-15.24" length="middle" rot="R180"/>
<pin name="SD_D4" x="38.1" y="-17.78" length="middle" rot="R180"/>
<pin name="NC_5" x="38.1" y="-20.32" length="middle" rot="R180"/>
<pin name="GROUND_PAD_(VSS)" x="7.62" y="30.48" length="middle" rot="R270"/>
<pin name="VDDA33" x="10.16" y="30.48" length="middle" rot="R270"/>
<pin name="RBIAS" x="12.7" y="30.48" length="middle" rot="R270"/>
<pin name="VDD18PLL" x="15.24" y="30.48" length="middle" rot="R270"/>
<pin name="XTAL1_(CLKIN)" x="17.78" y="30.48" length="middle" rot="R270"/>
<pin name="XTAL2" x="20.32" y="30.48" length="middle" rot="R270"/>
<pin name="TXD/SCK" x="22.86" y="30.48" length="middle" rot="R270"/>
<pin name="SD_WP" x="25.4" y="30.48" length="middle" rot="R270"/>
<pin name="NC_7" x="27.94" y="30.48" length="middle" rot="R270"/>
<pin name="TEST" x="30.48" y="30.48" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB2244-AEZG-06" prefix="IC">
<description>&lt;b&gt;USB 2.0 SD/MMC Flash Controller QFN36 Microchip USB2244-AEZG-06, USB Controller, 35MBps, USB 2.0, 3.3 V, 36-Pin QFN&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en562093"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="USB2244-AEZG-06" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN50P600X600X90-37N">
<connects>
<connect gate="G$1" pin="CRD_PWR" pad="21"/>
<connect gate="G$1" pin="GROUND_PAD_(VSS)" pad="37"/>
<connect gate="G$1" pin="LED" pad="1"/>
<connect gate="G$1" pin="NC_1" pad="12"/>
<connect gate="G$1" pin="NC_2" pad="15"/>
<connect gate="G$1" pin="NC_3" pad="16"/>
<connect gate="G$1" pin="NC_4" pad="17"/>
<connect gate="G$1" pin="NC_5" pad="19"/>
<connect gate="G$1" pin="NC_6" pad="24"/>
<connect gate="G$1" pin="NC_7" pad="29"/>
<connect gate="G$1" pin="RBIAS" pad="35"/>
<connect gate="G$1" pin="RESET_N" pad="18"/>
<connect gate="G$1" pin="RXD/SDA" pad="27"/>
<connect gate="G$1" pin="SD_CLK" pad="9"/>
<connect gate="G$1" pin="SD_CMD" pad="11"/>
<connect gate="G$1" pin="SD_D0" pad="5"/>
<connect gate="G$1" pin="SD_D1" pad="4"/>
<connect gate="G$1" pin="SD_D2" pad="25"/>
<connect gate="G$1" pin="SD_D3" pad="23"/>
<connect gate="G$1" pin="SD_D4" pad="20"/>
<connect gate="G$1" pin="SD_D5" pad="10"/>
<connect gate="G$1" pin="SD_D6" pad="8"/>
<connect gate="G$1" pin="SD_D7" pad="7"/>
<connect gate="G$1" pin="SD_NCD" pad="26"/>
<connect gate="G$1" pin="SD_WP" pad="30"/>
<connect gate="G$1" pin="TEST" pad="28"/>
<connect gate="G$1" pin="TXD/SCK" pad="31"/>
<connect gate="G$1" pin="USB+" pad="2"/>
<connect gate="G$1" pin="USB-" pad="3"/>
<connect gate="G$1" pin="VDD18" pad="13"/>
<connect gate="G$1" pin="VDD18PLL" pad="34"/>
<connect gate="G$1" pin="VDD33_1" pad="6"/>
<connect gate="G$1" pin="VDD33_2" pad="14"/>
<connect gate="G$1" pin="VDD33_3" pad="22"/>
<connect gate="G$1" pin="VDDA33" pad="36"/>
<connect gate="G$1" pin="XTAL1_(CLKIN)" pad="33"/>
<connect gate="G$1" pin="XTAL2" pad="32"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="70608248" constant="no"/>
<attribute name="ALLIED_PRICE-STOCK" value="https://www.alliedelec.com/microchip-technology-inc-usb2244-aezg-06/70608248/" constant="no"/>
<attribute name="DESCRIPTION" value="USB 2.0 SD/MMC Flash Controller QFN36 Microchip USB2244-AEZG-06, USB Controller, 35MBps, USB 2.0, 3.3 V, 36-Pin QFN" constant="no"/>
<attribute name="HEIGHT" value="0.9mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Microchip" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="USB2244-AEZG-06" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="886-USB2244-AEZG-06" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=886-USB2244-AEZG-06" constant="no"/>
<attribute name="RS_PART_NUMBER" value="7729616P" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="http://uk.rs-online.com/web/p/products/7729616P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="USB-A">
<description>&lt;CONN PLUG USB A 4POS SMD R/A&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="0480372200">
<description>&lt;b&gt;0480372200&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="3.5" y="10.175" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="1" y="10.175" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="-1" y="10.175" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="-3.5" y="10.175" dx="2" dy="1.2" layer="1" rot="R90"/>
<pad name="5" x="-2.25" y="7.575" drill="1.25" diameter="1.875"/>
<pad name="6" x="2.25" y="7.575" drill="1.25" diameter="1.875"/>
<pad name="7" x="-5.85" y="7.575" drill="2.5962" diameter="3.894"/>
<pad name="8" x="5.85" y="7.575" drill="2.5962" diameter="3.894"/>
<text x="0" y="0.5" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0.5" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-7.62" y1="8.575" x2="7.62" y2="8.575" width="0.2" layer="51"/>
<wire x1="7.62" y1="8.575" x2="7.62" y2="-10.175" width="0.2" layer="51"/>
<wire x1="7.62" y1="-10.175" x2="-7.62" y2="-10.175" width="0.2" layer="51"/>
<wire x1="-7.62" y1="-10.175" x2="-7.62" y2="8.575" width="0.2" layer="51"/>
<wire x1="-8.797" y1="12.175" x2="8.797" y2="12.175" width="0.1" layer="51"/>
<wire x1="8.797" y1="12.175" x2="8.797" y2="-11.175" width="0.1" layer="51"/>
<wire x1="8.797" y1="-11.175" x2="-8.797" y2="-11.175" width="0.1" layer="51"/>
<wire x1="-8.797" y1="-11.175" x2="-8.797" y2="12.175" width="0.1" layer="51"/>
<wire x1="-7.62" y1="5.825" x2="-7.62" y2="-10.175" width="0.1" layer="21"/>
<wire x1="-7.62" y1="-10.175" x2="7.62" y2="-10.175" width="0.1" layer="21"/>
<wire x1="7.62" y1="-10.175" x2="7.62" y2="5.825" width="0.1" layer="21"/>
<wire x1="-1" y1="8.575" x2="1" y2="8.575" width="0.1" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="0480372200">
<wire x1="5.08" y1="2.54" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<text x="16.51" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="16.51" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" length="middle"/>
<pin name="3" x="0" y="-2.54" length="middle"/>
<pin name="5" x="0" y="-5.08" length="middle"/>
<pin name="7" x="0" y="-7.62" length="middle"/>
<pin name="2" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="4" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="6" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="8" x="20.32" y="-7.62" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0480372200" prefix="J">
<description>&lt;b&gt;CONN PLUG USB A 4POS SMD R/A&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.molex.com/pdm_docs/sd/480372200_sd.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="0480372200" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0480372200">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="CONN PLUG USB A 4POS SMD R/A" constant="no"/>
<attribute name="HEIGHT" value="4mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Molex" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="0480372200" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="N/A" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=N%2FA" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TPS79533DCQR">
<description>&lt;Ultralow-Noise, High-PSRR, Fast, RF, 500-mA Low-Dropout Linear Regulators&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOT127P706X180-6N">
<description>&lt;b&gt;TPS79533DCQR&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-3" y="2.54" dx="2" dy="0.65" layer="1"/>
<smd name="2" x="-3" y="1.27" dx="2" dy="0.65" layer="1"/>
<smd name="3" x="-3" y="0" dx="2" dy="0.65" layer="1"/>
<smd name="4" x="-3" y="-1.27" dx="2" dy="0.65" layer="1"/>
<smd name="5" x="-3" y="-2.54" dx="2" dy="0.65" layer="1"/>
<smd name="6" x="3" y="0" dx="3.2" dy="2" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.25" y1="3.525" x2="4.25" y2="3.525" width="0.05" layer="51"/>
<wire x1="4.25" y1="3.525" x2="4.25" y2="-3.525" width="0.05" layer="51"/>
<wire x1="4.25" y1="-3.525" x2="-4.25" y2="-3.525" width="0.05" layer="51"/>
<wire x1="-4.25" y1="-3.525" x2="-4.25" y2="3.525" width="0.05" layer="51"/>
<wire x1="-1.75" y1="3.25" x2="1.75" y2="3.25" width="0.1" layer="51"/>
<wire x1="1.75" y1="3.25" x2="1.75" y2="-3.25" width="0.1" layer="51"/>
<wire x1="1.75" y1="-3.25" x2="-1.75" y2="-3.25" width="0.1" layer="51"/>
<wire x1="-1.75" y1="-3.25" x2="-1.75" y2="3.25" width="0.1" layer="51"/>
<wire x1="-1.75" y1="1.98" x2="-0.48" y2="3.25" width="0.1" layer="51"/>
<wire x1="-1.65" y1="3.25" x2="1.65" y2="3.25" width="0.2" layer="21"/>
<wire x1="1.65" y1="3.25" x2="1.65" y2="-3.25" width="0.2" layer="21"/>
<wire x1="1.65" y1="-3.25" x2="-1.65" y2="-3.25" width="0.2" layer="21"/>
<wire x1="-1.65" y1="-3.25" x2="-1.65" y2="3.25" width="0.2" layer="21"/>
<wire x1="-4" y1="3.215" x2="-2" y2="3.215" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="TPS79533DCQR">
<wire x1="5.08" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-7.62" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="26.67" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="26.67" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="EN" x="0" y="0" length="middle"/>
<pin name="IN" x="0" y="-2.54" length="middle"/>
<pin name="GND_1" x="0" y="-5.08" length="middle"/>
<pin name="OUT" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="FB" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="GND_2" x="30.48" y="-5.08" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TPS79533DCQR" prefix="IC">
<description>&lt;b&gt;Ultralow-Noise, High-PSRR, Fast, RF, 500-mA Low-Dropout Linear Regulators&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.ti.com/lit/gpn/tps795"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TPS79533DCQR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT127P706X180-6N">
<connects>
<connect gate="G$1" pin="EN" pad="1"/>
<connect gate="G$1" pin="FB" pad="5"/>
<connect gate="G$1" pin="GND_1" pad="3"/>
<connect gate="G$1" pin="GND_2" pad="6"/>
<connect gate="G$1" pin="IN" pad="2"/>
<connect gate="G$1" pin="OUT" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Ultralow-Noise, High-PSRR, Fast, RF, 500-mA Low-Dropout Linear Regulators" constant="no"/>
<attribute name="HEIGHT" value="1.8mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="TPS79533DCQR" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="595-TPS79533DCQR" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=595-TPS79533DCQR" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="5V">
<description>&lt;h3&gt;5V Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="3.3V">
<description>&lt;h3&gt;3.3V Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="5V" prefix="SUPPLY">
<description>&lt;h3&gt;5V Supply Symbol&lt;/h3&gt;
&lt;p&gt;Power supply symbol for a specifically-stated 5V source.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V" prefix="SUPPLY">
<description>&lt;h3&gt;3.3V Supply Symbol&lt;/h3&gt;
&lt;p&gt;Power supply symbol for a specifically-stated 3.3V source.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_Connectors">
<packages>
<package name="MF-MICROSD">
<description>&lt;b&gt;Description:&lt;/b&gt; Package for Micro SD Card Slot connector. Based on 4uconnector 15882.&lt;br&gt;</description>
<smd name="PIN1" x="-1.93875" y="3.15125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN2" x="-0.83875" y="2.95125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN3" x="0.26125" y="3.15125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN4" x="1.36125" y="3.35125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN5" x="2.46125" y="3.15125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN6" x="3.56125" y="3.35125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN7" x="4.66125" y="3.15125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN8" x="5.76125" y="3.15125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="G0" x="-6.58875" y="7.00125" dx="1.4" dy="1.9" layer="1"/>
<smd name="G1" x="6.56125" y="6.00125" dx="1.4" dy="1.9" layer="1"/>
<smd name="PIN9" x="4.96125" y="-7.14875" dx="1.8" dy="1.4" layer="1"/>
<smd name="PIN10" x="-0.73875" y="-7.14875" dx="1.8" dy="1.4" layer="1"/>
<wire x1="-7.025" y1="5.77625" x2="-7.025" y2="-7.625" width="0.127" layer="21"/>
<wire x1="-7.025" y1="-7.625" x2="-1.91375" y2="-7.625" width="0.127" layer="21"/>
<wire x1="6.19875" y1="-7.625" x2="7.025" y2="-7.625" width="0.127" layer="21"/>
<wire x1="7.025" y1="-7.625" x2="7.025" y2="4.67625" width="0.127" layer="21"/>
<wire x1="7.025" y1="7.28875" x2="7.025" y2="7.625" width="0.127" layer="21"/>
<wire x1="7.025" y1="7.625" x2="-5.65" y2="7.625" width="0.127" layer="21"/>
<text x="7.50125" y="-7.625" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<polygon width="0.127" layer="21">
<vertex x="1.79875" y="7.42625"/>
<vertex x="2.07375" y="7.42625"/>
<vertex x="2.07375" y="6.87625"/>
<vertex x="2.34875" y="6.87625"/>
<vertex x="1.93625" y="6.46375"/>
<vertex x="1.52375" y="6.87625"/>
<vertex x="1.79875" y="6.87625"/>
</polygon>
<wire x1="-2.32625" y1="7.625" x2="-2.32625" y2="8.225" width="0.127" layer="51"/>
<wire x1="6.19875" y1="7.625" x2="6.19875" y2="8.225" width="0.127" layer="51"/>
<wire x1="-2.32625" y1="8.225" x2="6.19875" y2="8.225" width="0.127" layer="51"/>
<wire x1="-2.32625" y1="8.225" x2="-2.32625" y2="9.225" width="0.127" layer="51"/>
<wire x1="-2.32625" y1="9.225" x2="6.19875" y2="9.225" width="0.127" layer="51"/>
<wire x1="6.19875" y1="9.225" x2="6.19875" y2="8.25125" width="0.127" layer="51"/>
<wire x1="-2.32625" y1="9.225" x2="-2.32625" y2="12.6075" width="0.127" layer="51"/>
<wire x1="-2.32625" y1="12.6075" x2="-2.00875" y2="12.925" width="0.127" layer="51" curve="-90"/>
<wire x1="-2.00875" y1="12.925" x2="5.88125" y2="12.925" width="0.127" layer="51"/>
<wire x1="5.88125" y1="12.925" x2="6.19875" y2="12.6075" width="0.127" layer="51" curve="-90"/>
<wire x1="6.19875" y1="12.6075" x2="6.19875" y2="9.225" width="0.127" layer="51"/>
<text x="6.47375" y="12.925" size="0.6096" layer="51" font="vector" ratio="16">Card Push Out</text>
<text x="6.47375" y="9.225" size="0.6096" layer="51" font="vector" ratio="16">Card Lock Position</text>
<text x="6.47375" y="8.225" size="0.6096" layer="51" font="vector" ratio="16">Card Push In</text>
<wire x1="0.4" y1="-7.625" x2="3.72375" y2="-7.625" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SD_CARD">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for SD Card Slot connector.&lt;br&gt;</description>
<wire x1="0" y1="0" x2="15.24" y2="0" width="0.127" layer="94"/>
<wire x1="15.24" y1="0" x2="15.24" y2="-27.94" width="0.127" layer="94"/>
<wire x1="15.24" y1="-27.94" x2="0" y2="-27.94" width="0.127" layer="94"/>
<wire x1="0" y1="-27.94" x2="0" y2="0" width="0.127" layer="94"/>
<pin name="RW" x="-2.54" y="-2.54" visible="off" length="short"/>
<pin name="CS" x="-2.54" y="-5.08" visible="off" length="short"/>
<pin name="DI" x="-2.54" y="-7.62" visible="off" length="short"/>
<pin name="SCLK" x="-2.54" y="-10.16" visible="off" length="short"/>
<pin name="D0" x="-2.54" y="-12.7" visible="off" length="short"/>
<pin name="IRQ" x="-2.54" y="-15.24" visible="off" length="short"/>
<pin name="3.3V" x="-2.54" y="-22.86" visible="off" length="short" direction="pwr"/>
<pin name="GND" x="-2.54" y="-25.4" visible="off" length="short" direction="pwr"/>
<text x="0" y="3.81" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="0" y="1.27" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<pin name="CRD_DTCT" x="-2.54" y="-20.32" visible="off" length="short"/>
<text x="0.762" y="-2.54" size="1.016" layer="97" font="vector" align="center-left">RW</text>
<text x="0.762" y="-5.08" size="1.016" layer="97" font="vector" align="center-left">CS</text>
<text x="0.762" y="-7.62" size="1.016" layer="97" font="vector" align="center-left">DI</text>
<text x="0.762" y="-10.16" size="1.016" layer="97" font="vector" align="center-left">SCLK</text>
<text x="0.762" y="-12.7" size="1.016" layer="97" font="vector" align="center-left">D0</text>
<text x="0.762" y="-15.24" size="1.016" layer="97" font="vector" align="center-left">IRQ</text>
<text x="0.762" y="-20.32" size="1.016" layer="97" font="vector" align="center-left">CARD_DETECT</text>
<text x="0.762" y="-22.86" size="1.016" layer="97" font="vector" align="center-left">3.3V</text>
<text x="0.762" y="-25.4" size="1.016" layer="97" font="vector" align="center-left">GND</text>
<wire x1="5.08" y1="-3.81" x2="5.588" y2="-3.81" width="0.127" layer="94"/>
<wire x1="5.588" y1="-3.81" x2="6.858" y2="-3.81" width="0.127" layer="94"/>
<wire x1="6.858" y1="-3.81" x2="8.128" y2="-3.81" width="0.127" layer="94"/>
<wire x1="8.128" y1="-3.81" x2="9.398" y2="-3.81" width="0.127" layer="94"/>
<wire x1="9.398" y1="-3.81" x2="10.668" y2="-3.81" width="0.127" layer="94"/>
<wire x1="10.668" y1="-3.81" x2="10.7" y2="-3.81" width="0.127" layer="94"/>
<wire x1="10.7" y1="-3.81" x2="12.7" y2="-5.81" width="0.127" layer="94"/>
<wire x1="12.7" y1="-5.81" x2="12.7" y2="-13.97" width="0.127" layer="94"/>
<wire x1="12.7" y1="-13.97" x2="11.43" y2="-13.97" width="0.127" layer="94"/>
<wire x1="11.43" y1="-13.97" x2="6.35" y2="-13.97" width="0.127" layer="94"/>
<wire x1="6.35" y1="-13.97" x2="5.08" y2="-13.97" width="0.127" layer="94"/>
<wire x1="5.08" y1="-13.97" x2="5.08" y2="-3.81" width="0.127" layer="94"/>
<wire x1="10.668" y1="-3.81" x2="10.668" y2="-5.08" width="0.127" layer="94"/>
<wire x1="9.398" y1="-3.81" x2="9.398" y2="-5.08" width="0.127" layer="94"/>
<wire x1="8.128" y1="-3.81" x2="8.128" y2="-5.08" width="0.127" layer="94"/>
<wire x1="6.858" y1="-3.81" x2="6.858" y2="-5.08" width="0.127" layer="94"/>
<wire x1="5.588" y1="-3.81" x2="5.588" y2="-5.08" width="0.127" layer="94"/>
<wire x1="6.35" y1="-13.97" x2="6.35" y2="-9.89" width="0.127" layer="94"/>
<wire x1="6.35" y1="-9.89" x2="7.35" y2="-8.89" width="0.127" layer="94" curve="-90"/>
<wire x1="7.35" y1="-8.89" x2="10.43" y2="-8.89" width="0.127" layer="94"/>
<wire x1="10.43" y1="-8.89" x2="11.43" y2="-9.89" width="0.127" layer="94" curve="-90"/>
<wire x1="11.43" y1="-9.89" x2="11.43" y2="-13.97" width="0.127" layer="94"/>
<text x="7.62" y="-1.27" size="1.016" layer="97" font="vector" align="center">SD CARD SLOT</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SD_CARD_SLOT" prefix="SD">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Connectors&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; SD Card Connector&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="SD_CARD" x="0" y="0"/>
</gates>
<devices>
<device name="_MICRO_RIGHT" package="MF-MICROSD">
<connects>
<connect gate="G$1" pin="3.3V" pad="PIN4"/>
<connect gate="G$1" pin="CRD_DTCT" pad="PIN9 PIN10"/>
<connect gate="G$1" pin="CS" pad="PIN2"/>
<connect gate="G$1" pin="D0" pad="PIN7"/>
<connect gate="G$1" pin="DI" pad="PIN3"/>
<connect gate="G$1" pin="GND" pad="G0 G1 PIN6"/>
<connect gate="G$1" pin="IRQ" pad="PIN8"/>
<connect gate="G$1" pin="RW" pad="PIN1"/>
<connect gate="G$1" pin="SCLK" pad="PIN5"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-MICROSD" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-MICROSD" constant="no"/>
<attribute name="VALUE" value="MF-MICROSD" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_Switches">
<packages>
<package name="TACT6MM">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 6MM Tact Switch Package.&lt;br/&gt;</description>
<smd name="P$1" x="-3.975" y="2.45" dx="1.55" dy="1.3" layer="1"/>
<smd name="P$2" x="3.975" y="2.45" dx="1.55" dy="1.3" layer="1"/>
<smd name="P$3" x="-3.975" y="-2.45" dx="1.55" dy="1.3" layer="1"/>
<smd name="P$4" x="3.975" y="-2.45" dx="1.55" dy="1.3" layer="1"/>
<wire x1="-5.2" y1="3.4" x2="-5.2" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-5.2" y1="-3.4" x2="5.2" y2="-3.4" width="0.127" layer="21"/>
<wire x1="5.2" y1="-3.4" x2="5.2" y2="3.4" width="0.127" layer="21"/>
<wire x1="5.2" y1="3.4" x2="-5.2" y2="3.4" width="0.127" layer="21"/>
<wire x1="-2.67" y1="2.4" x2="0" y2="2.4" width="0.127" layer="21"/>
<wire x1="0" y1="2.4" x2="2.67" y2="2.4" width="0.127" layer="21"/>
<wire x1="-2.67" y1="-2.4" x2="0" y2="-2.4" width="0.127" layer="21"/>
<wire x1="0" y1="-2.4" x2="2.67" y2="-2.4" width="0.127" layer="21"/>
<wire x1="0" y1="2.4" x2="0" y2="1" width="0.127" layer="21"/>
<wire x1="0" y1="-2.4" x2="0" y2="-1" width="0.127" layer="21"/>
<wire x1="0" y1="-1" x2="-0.8" y2="0.8" width="0.127" layer="21"/>
<text x="-5.2" y="3.6" size="1.016" layer="21" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="TACT4.2MM">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 4.2MM Tact Switch Package.Based off C&amp;K PTS 810 Series Tact Switch.&lt;br/&gt;</description>
<smd name="P$3" x="-2.075" y="-1.075" dx="0.65" dy="1.05" layer="1" rot="R90"/>
<smd name="P$4" x="2.075" y="-1.075" dx="0.65" dy="1.05" layer="1" rot="R90"/>
<smd name="P$2" x="2.075" y="1.075" dx="0.65" dy="1.05" layer="1" rot="R90"/>
<smd name="P$1" x="-2.075" y="1.075" dx="0.65" dy="1.05" layer="1" rot="R90"/>
<wire x1="-2.8" y1="1.6" x2="2.8" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.8" y1="1.6" x2="2.8" y2="-1.6" width="0.127" layer="21"/>
<wire x1="2.8" y1="-1.6" x2="-2.8" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-1.6" x2="-2.8" y2="1.6" width="0.127" layer="21"/>
<text x="-2.8" y="2" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<wire x1="-1.2" y1="1.2" x2="0" y2="1.2" width="0.127" layer="25"/>
<wire x1="0" y1="1.2" x2="1.2" y2="1.2" width="0.127" layer="25"/>
<wire x1="-1.2" y1="-1.2" x2="0" y2="-1.2" width="0.127" layer="25"/>
<wire x1="0" y1="-1.2" x2="1.2" y2="-1.2" width="0.127" layer="25"/>
<wire x1="0" y1="-1.2" x2="0" y2="-0.6" width="0.127" layer="25"/>
<wire x1="0" y1="1.2" x2="0" y2="0.6" width="0.127" layer="25"/>
<wire x1="0" y1="-0.6" x2="-0.4" y2="0.4" width="0.127" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="TACTSW">
<description>&lt;b&gt;Description:&lt;/b&gt; Tact Switch Symbol.&lt;br/&gt;</description>
<pin name="P$1" x="-2.54" y="2.54" visible="off" length="short"/>
<pin name="P$2" x="2.54" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="P$3" x="-2.54" y="-2.54" visible="off" length="short"/>
<pin name="P$4" x="2.54" y="-2.54" visible="off" length="short" rot="R180"/>
<text x="-2.54" y="7.62" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.016" y2="1.016" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TACT" prefix="SW" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Switches&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for Tact Switches. Manufacture part number (MFG#) can be added via Attributes.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="TACTSW" x="0" y="0"/>
</gates>
<devices>
<device name="_6MM" package="TACT6MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-SW-TACT-6MM" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-SW-TACT-6MM" constant="no"/>
<attribute name="VALUE" value="MF-SW-TACT-6MM" constant="no"/>
</technology>
</technologies>
</device>
<device name="_4.2MM" package="TACT4.2MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-SW-TACT-4.2MM" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-SW-TACT-4.2MM" constant="no"/>
<attribute name="VALUE" value="MF-SW-TACT-4.2MM" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_LEDs">
<packages>
<package name="LED0603">
<description>&lt;b&gt;Description:&lt;/b&gt; Footprint for Single LEDs in 0603&lt;br/&gt;</description>
<smd name="CATHODE" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="ANODE" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<wire x1="-1.5" y1="0.75" x2="-1.5" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="1.5" y2="-0.75" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.75" x2="-1.5" y2="0.75" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-1.5" y="0.75"/>
<vertex x="-1.7" y="0.75"/>
<vertex x="-1.7" y="-0.75"/>
<vertex x="-1.5" y="-0.75"/>
</polygon>
<text x="-1.7" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<polygon width="0.127" layer="21">
<vertex x="-0.15" y="0"/>
<vertex x="0.15" y="0.3"/>
<vertex x="0.15" y="-0.3"/>
</polygon>
</package>
<package name="LED0805">
<description>&lt;b&gt;Description:&lt;/b&gt; Footprint for Single LEDs in 0805&lt;br/&gt;</description>
<smd name="CATHODE" x="-1.25" y="0" dx="1.25" dy="1.1" layer="1" rot="R90"/>
<smd name="ANODE" x="1.25" y="0" dx="1.25" dy="1.1" layer="1" rot="R90"/>
<wire x1="-2.05" y1="0.85" x2="-2.05" y2="-0.85" width="0.127" layer="21"/>
<wire x1="-2.05" y1="-0.85" x2="2.05" y2="-0.85" width="0.127" layer="21"/>
<wire x1="2.05" y1="-0.85" x2="2.05" y2="0.85" width="0.127" layer="21"/>
<wire x1="2.05" y1="0.85" x2="-2.05" y2="0.85" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-2.05" y="0.85"/>
<vertex x="-2.25" y="0.85"/>
<vertex x="-2.25" y="-0.85"/>
<vertex x="-2.05" y="-0.85"/>
</polygon>
<text x="-2.25" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<polygon width="0.127" layer="21">
<vertex x="-0.15" y="0"/>
<vertex x="0.15" y="0.3"/>
<vertex x="0.15" y="-0.3"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for Single LEDs&lt;br/&gt;</description>
<wire x1="1.27" y1="1.524" x2="1.27" y2="-1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="-0.508" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.524" x2="-0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.524" x2="1.016" y2="0" width="0.254" layer="94"/>
<polygon width="0.254" layer="94">
<vertex x="-0.508" y="-1.524"/>
<vertex x="-0.508" y="1.524"/>
<vertex x="1.016" y="0"/>
</polygon>
<wire x1="-0.508" y1="2.286" x2="0.508" y2="3.302" width="0.254" layer="94"/>
<wire x1="0.508" y1="3.302" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="1.016" y2="4.826" width="0.254" layer="94"/>
<wire x1="1.016" y1="4.826" x2="0.508" y2="4.826" width="0.254" layer="94"/>
<wire x1="0.508" y1="4.826" x2="1.016" y2="4.318" width="0.254" layer="94"/>
<wire x1="1.016" y1="4.318" x2="1.016" y2="4.826" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.286" x2="1.778" y2="3.302" width="0.254" layer="94"/>
<wire x1="1.778" y1="3.302" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="2.286" y2="4.826" width="0.254" layer="94"/>
<wire x1="2.286" y1="4.826" x2="1.778" y2="4.826" width="0.254" layer="94"/>
<wire x1="1.778" y1="4.826" x2="2.286" y2="4.318" width="0.254" layer="94"/>
<wire x1="2.286" y1="4.318" x2="2.286" y2="4.826" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<pin name="ANODE" x="-2.54" y="0" visible="off" length="short" direction="pwr"/>
<pin name="CATHODE" x="2.54" y="0" visible="off" length="short" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED_SINGLE" prefix="D" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_LEDs&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for Single LED Packages. Manufacture part number (MFG#) can be added via Attributes.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-RED" package="LED0603">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-LED-0603-RED" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-LED-0603-RED" constant="no"/>
<attribute name="VALUE" value="MF-LED-0603-RED" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805-RED" package="LED0805">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-LED-0805-RED" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-LED-0805-RED" constant="no"/>
<attribute name="VALUE" value="MF-LED-0805-RED" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0603-GREEN" package="LED0603">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-LED-0603-GREEN" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-LED-0603-GREEN" constant="no"/>
<attribute name="VALUE" value="MF-LED-0603-GREEN" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805-GREEN" package="LED0805">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-LED-0805-GREEN" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-LED-0805-GREEN" constant="no"/>
<attribute name="VALUE" value="MF-LED-0805-GREEN" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0603" package="LED0603">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805" package="LED0805">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_Passives">
<packages>
<package name="R0402">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0402 Package for Resistors&lt;br/&gt;</description>
<smd name="P$1" x="-0.55" y="0" dx="0.5" dy="0.6" layer="1" rot="R180"/>
<smd name="P$2" x="0.55" y="0" dx="0.5" dy="0.6" layer="1" rot="R180"/>
<wire x1="-1.1" y1="0.55" x2="-1.1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.55" x2="1.1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.55" x2="1.1" y2="0.55" width="0.127" layer="21"/>
<wire x1="1.1" y1="0.55" x2="-1.1" y2="0.55" width="0.127" layer="21"/>
<text x="-1.1" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="R0603">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0603 Package for Resistors&lt;br/&gt;</description>
<smd name="P$1" x="-0.75" y="0" dx="0.6" dy="0.9" layer="1" rot="R180"/>
<smd name="P$2" x="0.75" y="0" dx="0.6" dy="0.9" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.7" x2="1.4" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.7" x2="1.4" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.4" y1="0.7" x2="-1.4" y2="0.7" width="0.127" layer="21"/>
<text x="-1.4" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="R0805">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0805 Package for Resistors&lt;br/&gt;</description>
<smd name="P$1" x="-0.95" y="0" dx="0.7" dy="1.3" layer="1" rot="R180"/>
<smd name="P$2" x="0.95" y="0" dx="0.7" dy="1.3" layer="1" rot="R180"/>
<wire x1="-1.8" y1="0.9" x2="-1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="0.9" x2="-1.8" y2="0.9" width="0.127" layer="21"/>
<text x="-1.8" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="R1206">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 1206 Package for Resistors&lt;br/&gt;</description>
<smd name="P$1" x="-1.45" y="0" dx="0.9" dy="1.6" layer="1" rot="R180"/>
<smd name="P$2" x="1.45" y="0" dx="0.9" dy="1.6" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.1" x2="-2.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.1" x2="2.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.1" x2="2.2" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.2" y1="1.1" x2="-2.2" y2="1.1" width="0.127" layer="21"/>
<text x="-2.2" y="1.3" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="R1210">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 1210 Package for Resistors&lt;br/&gt;</description>
<smd name="P$1" x="-1.45" y="0" dx="0.9" dy="2.5" layer="1" rot="R180"/>
<smd name="P$2" x="1.45" y="0" dx="0.9" dy="2.5" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.6" x2="-2.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.6" x2="2.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.6" x2="2.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.2" y1="1.6" x2="-2.2" y2="1.6" width="0.127" layer="21"/>
<text x="-2.2" y="1.8" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="C0402">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0402 Package for Capacitors&lt;br/&gt;</description>
<smd name="P$1" x="-0.55" y="0" dx="0.5" dy="0.6" layer="1" rot="R180"/>
<smd name="P$2" x="0.55" y="0" dx="0.5" dy="0.6" layer="1" rot="R180"/>
<wire x1="-1.1" y1="0.55" x2="-1.1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.55" x2="1.1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.55" x2="1.1" y2="0.55" width="0.127" layer="21"/>
<wire x1="1.1" y1="0.55" x2="-1.1" y2="0.55" width="0.127" layer="21"/>
<text x="-1.1" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="C0603">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0603 Package for Capacitors&lt;br/&gt;</description>
<smd name="P$1" x="-0.75" y="0" dx="0.6" dy="0.9" layer="1" rot="R180"/>
<smd name="P$2" x="0.75" y="0" dx="0.6" dy="0.9" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.7" x2="1.4" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.7" x2="1.4" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.4" y1="0.7" x2="-1.4" y2="0.7" width="0.127" layer="21"/>
<text x="-1.4" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="C0805">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0805 Package for Capacitors&lt;br/&gt;</description>
<smd name="P$1" x="-0.95" y="0" dx="0.7" dy="1.3" layer="1" rot="R180"/>
<smd name="P$2" x="0.95" y="0" dx="0.7" dy="1.3" layer="1" rot="R180"/>
<wire x1="-1.8" y1="0.9" x2="-1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="0.9" x2="-1.8" y2="0.9" width="0.127" layer="21"/>
<text x="-1.8" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="C1206">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 1206 Package for Capacitors&lt;br/&gt;</description>
<smd name="P$1" x="-1.45" y="0" dx="0.9" dy="1.6" layer="1" rot="R180"/>
<smd name="P$2" x="1.45" y="0" dx="0.9" dy="1.6" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.1" x2="-2.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.1" x2="2.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.1" x2="2.2" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.2" y1="1.1" x2="-2.2" y2="1.1" width="0.127" layer="21"/>
<text x="-2.2" y="1.3" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="C1210">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 1210 Package for Capacitors&lt;br/&gt;</description>
<smd name="P$1" x="-1.45" y="0" dx="0.9" dy="2.5" layer="1" rot="R180"/>
<smd name="P$2" x="1.45" y="0" dx="0.9" dy="2.5" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.6" x2="-2.2" y2="-1.575" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.575" x2="2.2" y2="-1.575" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.575" x2="2.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.2" y1="1.6" x2="-2.2" y2="1.6" width="0.127" layer="21"/>
<text x="-2.2" y="1.8" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Passives&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Symbol for Resistors&lt;br/&gt;</description>
<pin name="P$1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P$2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="1.016" y2="2.159" width="0.1524" layer="94"/>
<wire x1="1.016" y1="2.159" x2="-1.016" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="1.524" x2="1.016" y2="0.889" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0.889" x2="-1.016" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.254" x2="1.016" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-0.381" x2="-1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-1.016" x2="1.016" y2="-1.651" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.651" x2="-1.016" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.286" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="2.54" y="1.524" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="2.54" y="-1.524" size="1.016" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="CAPACITOR_NP">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Passives&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Symbol for Non-Polarized Capacitors&lt;br/&gt;</description>
<pin name="P$1" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P$2" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<text x="2.54" y="1.524" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="2.54" y="-1.524" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<wire x1="0" y1="2.54" x2="0" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.016" width="0.1524" layer="94"/>
<rectangle x1="-1.778" y1="0.508" x2="1.778" y2="1.27" layer="94"/>
<rectangle x1="-1.778" y1="-1.27" x2="1.778" y2="-0.508" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Passives&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for Resistors. Manufacture part number (MPN), Voltage, Tolerance, and Wattage Rating can be added via Attributes.  Check https://factory.macrofab.com/parts for the house parts list.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="R0402">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
<attribute name="WATTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0603" package="R0603">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
<attribute name="WATTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0805" package="R0805">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
<attribute name="WATTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_1206" package="R1206">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
<attribute name="WATTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_1210" package="R1210">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
<attribute name="WATTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR_NP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Passives&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for Non-Polarized Capacitors. Manufacture part number (MPN) can be added via Attributes. Check https://factory.macrofab.com/parts for the house parts list.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="CAPACITOR_NP" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="C0402">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIELECTRIC" value="" constant="no"/>
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0603" package="C0603">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIELECTRIC" value="" constant="no"/>
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0805" package="C0805">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIELECTRIC" value="" constant="no"/>
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_1206" package="C1206">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIELECTRIC" value="" constant="no"/>
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_1210" package="C1210">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIELECTRIC" value="" constant="no"/>
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ASCO-24.000MHZ-EK-T3">
<packages>
<package name="OSCSC320X250X100N">
<wire x1="1.25" y1="1.6" x2="1.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.25" y1="-1.6" x2="-1.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.25" y1="-1.6" x2="-1.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.25" y1="1.6" x2="1.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.61" y1="-1.96" x2="-1.61" y2="-1.96" width="0.05" layer="39"/>
<wire x1="-1.61" y1="-1.96" x2="-1.61" y2="1.96" width="0.05" layer="39"/>
<wire x1="-1.61" y1="1.96" x2="1.61" y2="1.96" width="0.05" layer="39"/>
<text x="-5.58883125" y="9.3143" size="1.892709375" layer="25">&gt;NAME</text>
<text x="-6.17916875" y="-10.8793" size="1.7684" layer="27">&gt;VALUE</text>
<circle x="-1.75" y="1.25" radius="0.086021875" width="0.127" layer="21"/>
<wire x1="1.61" y1="-1.96" x2="1.61" y2="1.96" width="0.05" layer="39"/>
<smd name="1" x="-0.85" y="1.1" dx="1.1" dy="0.98" layer="1" roundness="53" rot="R270"/>
<smd name="2" x="-0.85" y="-1.1" dx="1.1" dy="0.98" layer="1" roundness="53" rot="R270"/>
<smd name="3" x="0.85" y="-1.1" dx="1.1" dy="0.98" layer="1" roundness="53" rot="R270"/>
<smd name="4" x="0.85" y="1.1" dx="1.1" dy="0.98" layer="1" roundness="53" rot="R270"/>
</package>
</packages>
<symbols>
<symbol name="ASCO-24.000MHZ-EK-T3">
<wire x1="-12.7" y1="12.7" x2="12.7" y2="12.7" width="0.41" layer="94"/>
<wire x1="12.7" y1="12.7" x2="12.7" y2="-12.7" width="0.41" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="-12.7" y2="-12.7" width="0.41" layer="94"/>
<wire x1="-12.7" y1="-12.7" x2="-12.7" y2="12.7" width="0.41" layer="94"/>
<text x="-12.7" y="13.7" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-12.7" y="-16.7" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="TRI-STATE" x="-17.78" y="2.54" length="middle"/>
<pin name="VDD" x="17.78" y="10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="OUTPUT" x="17.78" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="GND" x="17.78" y="-7.62" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ASCO-24.000MHZ-EK-T3" prefix="U">
<description>None</description>
<gates>
<gate name="G$1" symbol="ASCO-24.000MHZ-EK-T3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="OSCSC320X250X100N">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="OUTPUT" pad="3"/>
<connect gate="G$1" pin="TRI-STATE" pad="1"/>
<connect gate="G$1" pin="VDD" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" SMD Programmable Crystal Oscillator 24MHz 3.3V 15pF 4-Pin SMD T/R "/>
<attribute name="MF" value="Abracon"/>
<attribute name="MP" value="ASCO-24.000MHZ-EK-T3"/>
<attribute name="PACKAGE" value="SMD-4 Abracon"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X04">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X4">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1.27MM">
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
</package>
<package name="MOLEX-1X4_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
</package>
<package name="1X04-SMD">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="1X04_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-4-PTH">
<wire x1="-4.5" y1="-5" x2="-5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5" x2="-5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-6.3" x2="-6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-6" y1="-6.3" x2="-6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-6" y1="1.1" x2="6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="6" y1="1.1" x2="6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="6" y1="-6.3" x2="5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-6.3" x2="5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-5" x2="4.5" y2="-5" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-5" drill="0.7"/>
<pad name="2" x="-1" y="-5" drill="0.7"/>
<pad name="3" x="1" y="-5" drill="0.7"/>
<pad name="4" x="3" y="-5" drill="0.7"/>
<text x="-2.27" y="0.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.27" y="-1" size="0.4064" layer="27">&gt;Value</text>
<text x="-3.4" y="-4.3" size="1.27" layer="51">+</text>
<text x="-1.4" y="-4.3" size="1.27" layer="51">-</text>
<text x="0.7" y="-4.1" size="0.8" layer="51">S</text>
<text x="2.7" y="-4.1" size="0.8" layer="51">S</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="6.8222" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1MM-RA">
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="1X04_SMD_STRAIGHT_COMBO">
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="1.25" x2="8.99" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="8.99" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-SMD_LONG">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="JST-4-PTH-VERT">
<wire x1="-4.95" y1="-2.25" x2="-4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-4.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="2" x="-1" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="3" x="1" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="4" x="3" y="-0.55" drill="0.7" diameter="1.6256"/>
<text x="-3" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="1" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="-1.4" y="0.75" size="1.27" layer="51">+</text>
<text x="0.6" y="0.75" size="1.27" layer="51">-</text>
<text x="2.7" y="0.95" size="0.8" layer="51">Y</text>
<text x="-3.3" y="0.95" size="0.8" layer="51">B</text>
</package>
<package name="1X04_SMD_RA_FEMALE">
<wire x1="-5.205" y1="4.25" x2="-5.205" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="4.25" x2="-5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="-4.25" x2="5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-5.205" y1="-4.25" x2="5.205" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-1.59" y1="6.8" x2="-0.95" y2="7.65" layer="51"/>
<rectangle x1="0.95" y1="6.8" x2="1.59" y2="7.65" layer="51"/>
<rectangle x1="-4.13" y1="6.8" x2="-3.49" y2="7.65" layer="51"/>
<smd name="3" x="1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="-1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-4.425" y="2.775" size="1" layer="27">&gt;Value</text>
<text x="-4.225" y="-3.395" size="1" layer="25">&gt;Name</text>
<rectangle x1="3.49" y1="6.8" x2="4.13" y2="7.65" layer="51"/>
<smd name="4" x="3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="M04">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M04" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 4&lt;/b&gt;
Standard 4-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="M04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.27MM" package="1X04-1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X04-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09916" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="1X04-1MM-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_STRAIGHT_COMBO" package="1X04_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_LONG" package="1X04-SMD_LONG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-4-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X04_SMD_RA_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="NINA-W102" deviceset="NINA-W102" device=""/>
<part name="IC2" library="USB2244-AEZG-06" deviceset="USB2244-AEZG-06" device=""/>
<part name="J1" library="USB-A" deviceset="0480372200" device=""/>
<part name="IC3" library="TPS79533DCQR" deviceset="TPS79533DCQR" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY1" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="SUPPLY2" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="SUPPLY3" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY4" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY5" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY6" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY7" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY8" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SD1" library="MF_Connectors" deviceset="SD_CARD_SLOT" device="_MICRO_RIGHT" value="MF-MICROSD"/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY9" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY10" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SW1" library="MF_Switches" deviceset="TACT" device="_4.2MM" value="MF-SW-TACT-4.2MM"/>
<part name="SW2" library="MF_Switches" deviceset="TACT" device="_4.2MM" value="MF-SW-TACT-4.2MM"/>
<part name="D1" library="MF_LEDs" deviceset="LED_SINGLE" device="-0603-RED" value="MF-LED-0603-GREEN"/>
<part name="D2" library="MF_LEDs" deviceset="LED_SINGLE" device="-0603-RED" value="MF-LED-0603-RED"/>
<part name="R7" library="MF_Passives" deviceset="RESISTOR" device="_0402" value="100K"/>
<part name="R4" library="MF_Passives" deviceset="RESISTOR" device="_0402" value="100K"/>
<part name="R1" library="MF_Passives" deviceset="RESISTOR" device="_0402" value="4.7K"/>
<part name="R2" library="MF_Passives" deviceset="RESISTOR" device="_0402" value="12K"/>
<part name="R5" library="MF_Passives" deviceset="RESISTOR" device="_0402" value="4.7K"/>
<part name="R3" library="MF_Passives" deviceset="RESISTOR" device="_0402" value="330"/>
<part name="C3" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0402" value="0.1uF"/>
<part name="C4" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0402" value="1uF"/>
<part name="C1" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0402" value="1uF"/>
<part name="U1" library="ASCO-24.000MHZ-EK-T3" deviceset="ASCO-24.000MHZ-EK-T3" device=""/>
<part name="C2" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0402" value="1uF"/>
<part name="C5" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0402" value="1uF"/>
<part name="C6" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0402" value="1uF"/>
<part name="C7" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0402" value="1uF"/>
<part name="C8" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0402" value="1uF"/>
<part name="C9" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0402" value="1uF"/>
<part name="C10" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0402" value="1uF"/>
<part name="C11" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0402" value="1uF"/>
<part name="JP1" library="SparkFun-Connectors" deviceset="M04" device="LOCK"/>
<part name="R6" library="MF_Passives" deviceset="RESISTOR" device="_0402" value="4.7K"/>
<part name="R8" library="MF_Passives" deviceset="RESISTOR" device="_0402" value="4.7K"/>
<part name="SUPPLY11" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="38.1" y="30.48" smashed="yes">
<attribute name="NAME" x="135.89" y="38.1" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="135.89" y="35.56" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="IC2" gate="G$1" x="190.5" y="10.16" smashed="yes">
<attribute name="NAME" x="225.044" y="-17.526" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="225.806" y="14.732" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="J1" gate="G$1" x="104.14" y="78.74" smashed="yes">
<attribute name="NAME" x="109.474" y="67.056" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="109.22" y="83.312" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="IC3" gate="G$1" x="139.7" y="78.74" smashed="yes">
<attribute name="NAME" x="145.542" y="69.85" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="145.288" y="82.804" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND1" gate="1" x="137.16" y="60.96" smashed="yes">
<attribute name="VALUE" x="134.62" y="58.42" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="101.6" y="78.74" smashed="yes">
<attribute name="VALUE" x="101.6" y="81.534" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="139.7" y="78.74" smashed="yes">
<attribute name="VALUE" x="139.7" y="81.534" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="170.18" y="78.74" smashed="yes">
<attribute name="VALUE" x="170.18" y="81.534" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND2" gate="1" x="190.5" y="33.02" smashed="yes" rot="MR0">
<attribute name="VALUE" x="193.04" y="30.48" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="SUPPLY4" gate="G$1" x="200.66" y="55.88" smashed="yes">
<attribute name="VALUE" x="200.66" y="58.674" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY5" gate="G$1" x="241.3" y="-2.54" smashed="yes">
<attribute name="VALUE" x="241.3" y="0.254" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY6" gate="G$1" x="177.8" y="-2.54" smashed="yes">
<attribute name="VALUE" x="177.8" y="0.254" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY7" gate="G$1" x="208.28" y="-43.18" smashed="yes" rot="R180">
<attribute name="VALUE" x="208.28" y="-45.974" size="1.778" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="GND3" gate="1" x="205.74" y="-38.1" smashed="yes" rot="MR0">
<attribute name="VALUE" x="208.28" y="-40.64" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="SUPPLY8" gate="G$1" x="20.32" y="12.7" smashed="yes">
<attribute name="VALUE" x="20.32" y="15.494" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND4" gate="1" x="12.7" y="-10.16" smashed="yes" rot="MR0">
<attribute name="VALUE" x="15.24" y="-12.7" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND5" gate="1" x="139.7" y="-30.48" smashed="yes" rot="MR0">
<attribute name="VALUE" x="142.24" y="-33.02" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="SD1" gate="G$1" x="30.48" y="81.28" smashed="yes">
<attribute name="NAME" x="30.48" y="85.09" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="30.48" y="82.55" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="GND6" gate="1" x="27.94" y="50.8" smashed="yes">
<attribute name="VALUE" x="25.4" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY9" gate="G$1" x="12.7" y="58.42" smashed="yes">
<attribute name="VALUE" x="12.7" y="61.214" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY10" gate="G$1" x="152.4" y="35.56" smashed="yes">
<attribute name="VALUE" x="152.4" y="38.354" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SW1" gate="G$1" x="152.4" y="5.08" smashed="yes">
<attribute name="NAME" x="149.86" y="-1.27" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="149.86" y="0" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="SW2" gate="G$1" x="20.32" y="-25.4" smashed="yes">
<attribute name="NAME" x="17.78" y="-17.78" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="17.78" y="-20.32" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="D1" gate="G$1" x="177.8" y="22.86" smashed="yes" rot="MR90">
<attribute name="NAME" x="179.324" y="20.066" size="1.016" layer="95" font="vector" rot="MR90" align="top-left"/>
<attribute name="VALUE" x="180.34" y="17.78" size="1.016" layer="96" font="vector" rot="MR90"/>
</instance>
<instance part="D2" gate="G$1" x="25.4" y="-7.62" smashed="yes" rot="MR0">
<attribute name="NAME" x="27.94" y="-2.794" size="1.016" layer="95" font="vector" rot="MR0" align="top-left"/>
<attribute name="VALUE" x="27.94" y="-5.588" size="1.016" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="R7" gate="G$1" x="152.4" y="30.48" smashed="yes">
<attribute name="NAME" x="154.94" y="32.004" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="154.94" y="28.956" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="R4" gate="G$1" x="218.44" y="-38.1" smashed="yes">
<attribute name="NAME" x="220.98" y="-36.576" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="220.98" y="-39.624" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="R1" gate="G$1" x="182.88" y="10.16" smashed="yes" rot="R90">
<attribute name="NAME" x="181.356" y="12.7" size="1.016" layer="95" font="vector" rot="R90" align="top-left"/>
<attribute name="VALUE" x="184.404" y="12.7" size="1.016" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="177.8" y="40.64" smashed="yes">
<attribute name="NAME" x="180.34" y="42.164" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="180.34" y="39.116" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="R5" gate="G$1" x="33.02" y="-7.62" smashed="yes" rot="R90">
<attribute name="NAME" x="31.496" y="-5.08" size="1.016" layer="95" font="vector" rot="R90" align="top-left"/>
<attribute name="VALUE" x="34.544" y="-5.08" size="1.016" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R3" gate="G$1" x="127" y="68.58" smashed="yes">
<attribute name="NAME" x="125.984" y="65.786" size="1.016" layer="95" font="vector" rot="R180" align="top-left"/>
<attribute name="VALUE" x="125.984" y="65.024" size="1.016" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="C3" gate="G$1" x="132.08" y="68.58" smashed="yes">
<attribute name="NAME" x="134.62" y="70.104" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="134.62" y="67.056" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="C4" gate="G$1" x="205.74" y="-33.02" smashed="yes">
<attribute name="NAME" x="208.28" y="-31.496" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="208.28" y="-34.544" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="C1" gate="G$1" x="182.88" y="40.64" smashed="yes">
<attribute name="NAME" x="185.42" y="42.164" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="185.42" y="39.116" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="U1" gate="G$1" x="203.2" y="78.74" smashed="yes" rot="R270">
<attribute name="NAME" x="216.9" y="91.44" size="2.0828" layer="95" ratio="10" rot="SR270"/>
<attribute name="VALUE" x="186.5" y="91.44" size="2.0828" layer="96" ratio="10" rot="SR270"/>
</instance>
<instance part="C2" gate="G$1" x="193.04" y="53.34" smashed="yes">
<attribute name="NAME" x="195.58" y="54.864" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="195.58" y="51.816" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="C5" gate="G$1" x="241.3" y="-5.08" smashed="yes">
<attribute name="NAME" x="243.84" y="-3.556" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="243.84" y="-6.604" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="C6" gate="G$1" x="177.8" y="-5.08" smashed="yes">
<attribute name="NAME" x="180.34" y="-3.556" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="180.34" y="-6.604" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="C7" gate="G$1" x="213.36" y="-40.64" smashed="yes">
<attribute name="NAME" x="215.9" y="-39.116" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="215.9" y="-42.164" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="C8" gate="G$1" x="223.52" y="-33.02" smashed="yes">
<attribute name="NAME" x="226.06" y="-31.496" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="226.06" y="-34.544" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="C9" gate="G$1" x="12.7" y="55.88" smashed="yes">
<attribute name="NAME" x="15.24" y="57.404" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="15.24" y="54.356" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="C10" gate="G$1" x="27.94" y="5.08" smashed="yes">
<attribute name="NAME" x="30.48" y="6.604" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="30.48" y="3.556" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="C11" gate="G$1" x="20.32" y="5.08" smashed="yes">
<attribute name="NAME" x="22.86" y="6.604" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="22.86" y="3.556" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="JP1" gate="G$1" x="88.9" y="58.42" smashed="yes" rot="R180">
<attribute name="VALUE" x="93.98" y="66.04" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="93.98" y="50.038" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="R6" gate="G$1" x="66.04" y="58.42" smashed="yes">
<attribute name="NAME" x="68.58" y="59.944" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="68.58" y="56.896" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="R8" gate="G$1" x="60.96" y="60.96" smashed="yes">
<attribute name="NAME" x="63.5" y="62.484" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="63.5" y="59.436" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="SUPPLY11" gate="G$1" x="63.5" y="66.04" smashed="yes">
<attribute name="VALUE" x="63.5" y="68.834" size="1.778" layer="96" align="bottom-center"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="IC3" gate="G$1" pin="GND_1"/>
<wire x1="139.7" y1="73.66" x2="139.7" y2="63.5" width="0.1524" layer="91"/>
<wire x1="139.7" y1="63.5" x2="137.16" y2="63.5" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="GND_2"/>
<wire x1="170.18" y1="73.66" x2="170.18" y2="63.5" width="0.1524" layer="91"/>
<wire x1="170.18" y1="63.5" x2="139.7" y2="63.5" width="0.1524" layer="91"/>
<junction x="139.7" y="63.5"/>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="124.46" y1="76.2" x2="137.16" y2="76.2" width="0.1524" layer="91"/>
<wire x1="137.16" y1="76.2" x2="137.16" y2="63.5" width="0.1524" layer="91"/>
<junction x="137.16" y="63.5"/>
<pinref part="R3" gate="G$1" pin="P$2"/>
<junction x="137.16" y="63.5"/>
<wire x1="137.16" y1="63.5" x2="132.08" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="P$2"/>
<wire x1="132.08" y1="63.5" x2="127" y2="63.5" width="0.1524" layer="91"/>
<wire x1="132.08" y1="66.04" x2="132.08" y2="63.5" width="0.1524" layer="91"/>
<junction x="132.08" y="63.5"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GROUND_PAD_(VSS)"/>
<wire x1="198.12" y1="40.64" x2="190.5" y2="40.64" width="0.1524" layer="91"/>
<wire x1="190.5" y1="40.64" x2="190.5" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="177.8" y1="35.56" x2="182.88" y2="35.56" width="0.1524" layer="91"/>
<junction x="190.5" y="35.56"/>
<pinref part="IC2" gate="G$1" pin="TEST"/>
<wire x1="182.88" y1="35.56" x2="187.96" y2="35.56" width="0.1524" layer="91"/>
<wire x1="187.96" y1="35.56" x2="190.5" y2="35.56" width="0.1524" layer="91"/>
<wire x1="220.98" y1="40.64" x2="220.98" y2="48.26" width="0.1524" layer="91"/>
<wire x1="220.98" y1="48.26" x2="190.5" y2="48.26" width="0.1524" layer="91"/>
<wire x1="190.5" y1="48.26" x2="190.5" y2="40.64" width="0.1524" layer="91"/>
<junction x="190.5" y="40.64"/>
<pinref part="R2" gate="G$1" pin="P$2"/>
<pinref part="C1" gate="G$1" pin="P$2"/>
<wire x1="182.88" y1="38.1" x2="182.88" y2="35.56" width="0.1524" layer="91"/>
<junction x="182.88" y="35.56"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="195.58" y1="60.96" x2="187.96" y2="60.96" width="0.1524" layer="91"/>
<wire x1="187.96" y1="60.96" x2="187.96" y2="35.56" width="0.1524" layer="91"/>
<junction x="187.96" y="35.56"/>
<pinref part="C2" gate="G$1" pin="P$2"/>
<wire x1="193.04" y1="50.8" x2="193.04" y2="35.56" width="0.1524" layer="91"/>
<wire x1="193.04" y1="35.56" x2="190.5" y2="35.56" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="CATHODE"/>
<wire x1="177.8" y1="25.4" x2="177.8" y2="35.56" width="0.1524" layer="91"/>
<junction x="177.8" y="35.56"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND_1"/>
<wire x1="38.1" y1="17.78" x2="12.7" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND_2"/>
<wire x1="38.1" y1="2.54" x2="27.94" y2="2.54" width="0.1524" layer="91"/>
<wire x1="27.94" y1="2.54" x2="12.7" y2="2.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="2.54" x2="12.7" y2="17.78" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-7.62" x2="12.7" y2="-2.54" width="0.1524" layer="91"/>
<junction x="12.7" y="2.54"/>
<pinref part="GND4" gate="1" pin="GND"/>
<junction x="12.7" y="-7.62"/>
<pinref part="SW2" gate="G$1" pin="P$4"/>
<wire x1="12.7" y1="-2.54" x2="12.7" y2="2.54" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-27.94" x2="12.7" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-27.94" x2="12.7" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="SW2" gate="G$1" pin="P$3"/>
<wire x1="22.86" y1="-27.94" x2="17.78" y2="-27.94" width="0.1524" layer="91"/>
<junction x="22.86" y="-27.94"/>
<pinref part="C10" gate="G$1" pin="P$2"/>
<junction x="27.94" y="2.54"/>
<pinref part="C11" gate="G$1" pin="P$2"/>
<wire x1="20.32" y1="2.54" x2="12.7" y2="2.54" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND_3"/>
<wire x1="38.1" y1="-2.54" x2="12.7" y2="-2.54" width="0.1524" layer="91"/>
<junction x="12.7" y="-2.54"/>
<pinref part="D2" gate="G$1" pin="CATHODE"/>
<wire x1="22.86" y1="-7.62" x2="12.7" y2="-7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND_17"/>
<pinref part="IC1" gate="G$1" pin="GND_16"/>
<pinref part="IC1" gate="G$1" pin="GND_15"/>
<pinref part="IC1" gate="G$1" pin="GND_14"/>
<pinref part="IC1" gate="G$1" pin="GND_13"/>
<pinref part="IC1" gate="G$1" pin="GND_12"/>
<pinref part="IC1" gate="G$1" pin="GND_11"/>
<pinref part="IC1" gate="G$1" pin="GND_10"/>
<pinref part="IC1" gate="G$1" pin="GND_9"/>
<pinref part="IC1" gate="G$1" pin="GND_8"/>
<pinref part="IC1" gate="G$1" pin="GND_7"/>
<pinref part="IC1" gate="G$1" pin="GND_6"/>
<junction x="139.7" y="0"/>
<pinref part="IC1" gate="G$1" pin="GPIO_36/_JTAG_TDI/"/>
<wire x1="139.7" y1="2.54" x2="139.7" y2="0" width="0.1524" layer="91"/>
<wire x1="139.7" y1="0" x2="139.7" y2="-2.54" width="0.1524" layer="91"/>
<junction x="139.7" y="-2.54"/>
<wire x1="139.7" y1="-2.54" x2="139.7" y2="-5.08" width="0.1524" layer="91"/>
<junction x="139.7" y="-5.08"/>
<wire x1="139.7" y1="-5.08" x2="139.7" y2="-7.62" width="0.1524" layer="91"/>
<junction x="139.7" y="-7.62"/>
<wire x1="139.7" y1="-7.62" x2="139.7" y2="-10.16" width="0.1524" layer="91"/>
<junction x="139.7" y="-10.16"/>
<wire x1="139.7" y1="-10.16" x2="139.7" y2="-12.7" width="0.1524" layer="91"/>
<junction x="139.7" y="-12.7"/>
<wire x1="139.7" y1="-12.7" x2="139.7" y2="-15.24" width="0.1524" layer="91"/>
<junction x="139.7" y="-15.24"/>
<wire x1="139.7" y1="-15.24" x2="139.7" y2="-17.78" width="0.1524" layer="91"/>
<junction x="139.7" y="-17.78"/>
<wire x1="139.7" y1="-17.78" x2="139.7" y2="-20.32" width="0.1524" layer="91"/>
<junction x="139.7" y="-20.32"/>
<wire x1="139.7" y1="-20.32" x2="139.7" y2="-22.86" width="0.1524" layer="91"/>
<junction x="139.7" y="-22.86"/>
<wire x1="139.7" y1="-22.86" x2="139.7" y2="-25.4" width="0.1524" layer="91"/>
<junction x="139.7" y="-25.4"/>
<wire x1="139.7" y1="-25.4" x2="139.7" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<junction x="139.7" y="-27.94"/>
<pinref part="IC1" gate="G$1" pin="GND_4"/>
<wire x1="139.7" y1="27.94" x2="142.24" y2="27.94" width="0.1524" layer="91"/>
<wire x1="142.24" y1="27.94" x2="142.24" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND_5"/>
<wire x1="142.24" y1="17.78" x2="139.7" y2="17.78" width="0.1524" layer="91"/>
<wire x1="142.24" y1="2.54" x2="142.24" y2="17.78" width="0.1524" layer="91"/>
<junction x="142.24" y="17.78"/>
<pinref part="IC1" gate="G$1" pin="GPIO_25/_RMII_MDCLK/"/>
<wire x1="139.7" y1="30.48" x2="139.7" y2="27.94" width="0.1524" layer="91"/>
<junction x="139.7" y="27.94"/>
<wire x1="142.24" y1="2.54" x2="139.7" y2="2.54" width="0.1524" layer="91"/>
<junction x="139.7" y="2.54"/>
<pinref part="SW1" gate="G$1" pin="P$4"/>
<pinref part="SW1" gate="G$1" pin="P$3"/>
<wire x1="154.94" y1="2.54" x2="149.86" y2="2.54" width="0.1524" layer="91"/>
<wire x1="149.86" y1="2.54" x2="142.24" y2="2.54" width="0.1524" layer="91"/>
<junction x="149.86" y="2.54"/>
<junction x="142.24" y="2.54"/>
</segment>
<segment>
<pinref part="SD1" gate="G$1" pin="GND"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="27.94" y1="53.34" x2="27.94" y2="55.88" width="0.1524" layer="91"/>
<junction x="27.94" y="53.34"/>
<pinref part="C9" gate="G$1" pin="P$2"/>
<wire x1="12.7" y1="53.34" x2="27.94" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="P$2"/>
<pinref part="C6" gate="G$1" pin="P$2"/>
<wire x1="177.8" y1="-7.62" x2="177.8" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="177.8" y1="-35.56" x2="205.74" y2="-35.56" width="0.1524" layer="91"/>
<junction x="205.74" y="-35.56"/>
<pinref part="C5" gate="G$1" pin="P$2"/>
<wire x1="241.3" y1="-7.62" x2="241.3" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="241.3" y1="-35.56" x2="223.52" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="P$1"/>
<wire x1="223.52" y1="-35.56" x2="213.36" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-35.56" x2="205.74" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-38.1" x2="213.36" y2="-35.56" width="0.1524" layer="91"/>
<junction x="213.36" y="-35.56"/>
<pinref part="C8" gate="G$1" pin="P$2"/>
<junction x="223.52" y="-35.56"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="83.82" y1="58.42" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<label x="78.74" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DP" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="104.14" y1="76.2" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<label x="101.6" y="76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="USB+"/>
<wire x1="190.5" y1="7.62" x2="187.96" y2="7.62" width="0.1524" layer="91"/>
<label x="187.96" y="7.62" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DM" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="124.46" y1="78.74" x2="127" y2="78.74" width="0.1524" layer="91"/>
<label x="127" y="78.74" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="USB-"/>
<wire x1="190.5" y1="5.08" x2="187.96" y2="5.08" width="0.1524" layer="91"/>
<label x="187.96" y="5.08" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="104.14" y1="78.74" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="G$1" pin="5V"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="EN"/>
<pinref part="IC3" gate="G$1" pin="IN"/>
<wire x1="139.7" y1="76.2" x2="139.7" y2="78.74" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="G$1" pin="5V"/>
<junction x="139.7" y="78.74"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<wire x1="83.82" y1="60.96" x2="78.74" y2="60.96" width="0.1524" layer="91"/>
<label x="78.74" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="OUT"/>
<pinref part="SUPPLY3" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VDDA33"/>
<wire x1="200.66" y1="40.64" x2="200.66" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SUPPLY4" gate="G$1" pin="3.3V"/>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="200.66" y1="50.8" x2="200.66" y2="55.88" width="0.1524" layer="91"/>
<wire x1="200.66" y1="50.8" x2="213.36" y2="50.8" width="0.1524" layer="91"/>
<wire x1="213.36" y1="50.8" x2="213.36" y2="60.96" width="0.1524" layer="91"/>
<junction x="200.66" y="50.8"/>
<pinref part="C2" gate="G$1" pin="P$1"/>
<wire x1="200.66" y1="55.88" x2="193.04" y2="55.88" width="0.1524" layer="91"/>
<junction x="200.66" y="55.88"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VDD33_3"/>
<wire x1="228.6" y1="-2.54" x2="241.3" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="G$1" pin="3.3V"/>
<pinref part="C5" gate="G$1" pin="P$1"/>
<junction x="241.3" y="-2.54"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VDD33_1"/>
<wire x1="190.5" y1="-2.54" x2="177.8" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="SUPPLY6" gate="G$1" pin="3.3V"/>
<pinref part="C6" gate="G$1" pin="P$1"/>
<junction x="177.8" y="-2.54"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VDD33_2"/>
<wire x1="208.28" y1="-30.48" x2="208.28" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="SUPPLY7" gate="G$1" pin="3.3V"/>
<pinref part="R4" gate="G$1" pin="P$2"/>
<wire x1="218.44" y1="-43.18" x2="213.36" y2="-43.18" width="0.1524" layer="91"/>
<junction x="208.28" y="-43.18"/>
<pinref part="C7" gate="G$1" pin="P$2"/>
<wire x1="213.36" y1="-43.18" x2="208.28" y2="-43.18" width="0.1524" layer="91"/>
<junction x="213.36" y="-43.18"/>
</segment>
<segment>
<pinref part="SUPPLY8" gate="G$1" pin="3.3V"/>
<wire x1="20.32" y1="12.7" x2="20.32" y2="7.62" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<pinref part="IC1" gate="G$1" pin="VCC_IO"/>
<wire x1="38.1" y1="10.16" x2="38.1" y2="7.62" width="0.1524" layer="91"/>
<wire x1="20.32" y1="7.62" x2="27.94" y2="7.62" width="0.1524" layer="91"/>
<junction x="38.1" y="7.62"/>
<pinref part="C10" gate="G$1" pin="P$1"/>
<wire x1="27.94" y1="7.62" x2="38.1" y2="7.62" width="0.1524" layer="91"/>
<junction x="27.94" y="7.62"/>
<pinref part="C11" gate="G$1" pin="P$1"/>
<junction x="20.32" y="7.62"/>
</segment>
<segment>
<pinref part="SUPPLY9" gate="G$1" pin="3.3V"/>
<pinref part="SD1" gate="G$1" pin="3.3V"/>
<wire x1="12.7" y1="58.42" x2="27.94" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="P$1"/>
<junction x="12.7" y="58.42"/>
</segment>
<segment>
<pinref part="SUPPLY10" gate="G$1" pin="3.3V"/>
<pinref part="R7" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="P$1"/>
<wire x1="60.96" y1="66.04" x2="63.5" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="P$1"/>
<wire x1="63.5" y1="66.04" x2="66.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="66.04" y1="66.04" x2="66.04" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="G$1" pin="3.3V"/>
<junction x="63.5" y="66.04"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VDD18PLL"/>
<wire x1="182.88" y1="43.18" x2="205.74" y2="43.18" width="0.1524" layer="91"/>
<wire x1="205.74" y1="43.18" x2="205.74" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="RBIAS"/>
<wire x1="177.8" y1="45.72" x2="203.2" y2="45.72" width="0.1524" layer="91"/>
<wire x1="203.2" y1="45.72" x2="203.2" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RESET_N"/>
<wire x1="38.1" y1="-15.24" x2="20.32" y2="-15.24" width="0.1524" layer="91"/>
<label x="20.32" y="-15.24" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="SW2" gate="G$1" pin="P$1"/>
<wire x1="20.32" y1="-15.24" x2="20.32" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-22.86" x2="17.78" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="SW2" gate="G$1" pin="P$2"/>
<wire x1="17.78" y1="-22.86" x2="22.86" y2="-22.86" width="0.1524" layer="91"/>
<junction x="17.78" y="-22.86"/>
</segment>
</net>
<net name="SD_NCD" class="0">
<segment>
<pinref part="SD1" gate="G$1" pin="CRD_DTCT"/>
<wire x1="27.94" y1="60.96" x2="25.4" y2="60.96" width="0.1524" layer="91"/>
<label x="25.4" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="SD_NCD"/>
<wire x1="228.6" y1="7.62" x2="231.14" y2="7.62" width="0.1524" layer="91"/>
<label x="231.14" y="7.62" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPIO_32/_JTAG_TDO"/>
<wire x1="139.7" y1="12.7" x2="144.78" y2="12.7" width="0.1524" layer="91"/>
<label x="144.78" y="12.7" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="SD_D1" class="0">
<segment>
<pinref part="SD1" gate="G$1" pin="IRQ"/>
<wire x1="27.94" y1="66.04" x2="25.4" y2="66.04" width="0.1524" layer="91"/>
<label x="25.4" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="SD_D1"/>
<wire x1="190.5" y1="2.54" x2="187.96" y2="2.54" width="0.1524" layer="91"/>
<label x="187.96" y="2.54" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SD_CLK" class="0">
<segment>
<pinref part="SD1" gate="G$1" pin="SCLK"/>
<wire x1="27.94" y1="71.12" x2="25.4" y2="71.12" width="0.1524" layer="91"/>
<label x="25.4" y="71.12" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="SD_CLK"/>
<wire x1="190.5" y1="-10.16" x2="187.96" y2="-10.16" width="0.1524" layer="91"/>
<label x="187.96" y="-10.16" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPIO_29/_SPI_V_CLK"/>
<wire x1="139.7" y1="20.32" x2="144.78" y2="20.32" width="0.1524" layer="91"/>
<label x="144.78" y="20.32" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SD_CMD" class="0">
<segment>
<pinref part="SD1" gate="G$1" pin="DI"/>
<wire x1="27.94" y1="73.66" x2="25.4" y2="73.66" width="0.1524" layer="91"/>
<label x="25.4" y="73.66" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="SD_CMD"/>
<wire x1="200.66" y1="-30.48" x2="200.66" y2="-33.02" width="0.1524" layer="91"/>
<label x="200.66" y="-33.02" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPIO_1/_SPI_V_DI"/>
<wire x1="38.1" y1="30.48" x2="35.56" y2="30.48" width="0.1524" layer="91"/>
<label x="35.56" y="30.48" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SD_D3" class="0">
<segment>
<pinref part="SD1" gate="G$1" pin="CS"/>
<wire x1="27.94" y1="76.2" x2="25.4" y2="76.2" width="0.1524" layer="91"/>
<label x="25.4" y="76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="SD_D3"/>
<wire x1="228.6" y1="0" x2="231.14" y2="0" width="0.1524" layer="91"/>
<label x="231.14" y="0" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPIO_28/_SPI_V_CS"/>
<wire x1="139.7" y1="22.86" x2="144.78" y2="22.86" width="0.1524" layer="91"/>
<label x="144.78" y="22.86" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SD_D2" class="0">
<segment>
<pinref part="SD1" gate="G$1" pin="RW"/>
<wire x1="27.94" y1="78.74" x2="25.4" y2="78.74" width="0.1524" layer="91"/>
<label x="25.4" y="78.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="SD_D2"/>
<wire x1="228.6" y1="5.08" x2="231.14" y2="5.08" width="0.1524" layer="91"/>
<label x="231.14" y="5.08" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SD_D0" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="SD_D0"/>
<wire x1="190.5" y1="0" x2="187.96" y2="0" width="0.1524" layer="91"/>
<label x="187.96" y="0" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SD1" gate="G$1" pin="D0"/>
<wire x1="27.94" y1="68.58" x2="25.4" y2="68.58" width="0.1524" layer="91"/>
<label x="25.4" y="68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPIO_21/_UART_CTS/_RMII_TXD0/_SPI_V_DO/"/>
<wire x1="38.1" y1="-20.32" x2="35.56" y2="-20.32" width="0.1524" layer="91"/>
<label x="35.56" y="-20.32" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="FLASH" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPIO_27/_RMII_CLK/_SYS_BOOT"/>
<wire x1="154.94" y1="25.4" x2="152.4" y2="25.4" width="0.1524" layer="91"/>
<label x="152.4" y="25.4" size="1.27" layer="95" xref="yes"/>
<pinref part="SW1" gate="G$1" pin="P$2"/>
<wire x1="152.4" y1="25.4" x2="139.7" y2="25.4" width="0.1524" layer="91"/>
<wire x1="154.94" y1="25.4" x2="154.94" y2="7.62" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="P$1"/>
<wire x1="149.86" y1="7.62" x2="154.94" y2="7.62" width="0.1524" layer="91"/>
<junction x="154.94" y="7.62"/>
<pinref part="R7" gate="G$1" pin="P$2"/>
<junction x="152.4" y="25.4"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPIO_22/_UART_TXD"/>
<wire x1="38.1" y1="-22.86" x2="35.56" y2="-22.86" width="0.1524" layer="91"/>
<label x="35.56" y="-22.86" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="4"/>
<label x="78.74" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R6" gate="G$1" pin="P$2"/>
<wire x1="83.82" y1="53.34" x2="66.04" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPIO_23/_UART_RXD"/>
<wire x1="38.1" y1="-25.4" x2="35.56" y2="-25.4" width="0.1524" layer="91"/>
<label x="35.56" y="-25.4" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="3"/>
<label x="78.74" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R8" gate="G$1" pin="P$2"/>
<wire x1="83.82" y1="55.88" x2="60.96" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="P$1"/>
<pinref part="IC2" gate="G$1" pin="RESET_N"/>
<wire x1="218.44" y1="-33.02" x2="218.44" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="P$1"/>
<wire x1="223.52" y1="-30.48" x2="218.44" y2="-30.48" width="0.1524" layer="91"/>
<junction x="218.44" y="-30.48"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="7"/>
<pinref part="J1" gate="G$1" pin="5"/>
<wire x1="104.14" y1="73.66" x2="104.14" y2="71.12" width="0.1524" layer="91"/>
<wire x1="104.14" y1="71.12" x2="104.14" y2="68.58" width="0.1524" layer="91"/>
<wire x1="104.14" y1="68.58" x2="124.46" y2="68.58" width="0.1524" layer="91"/>
<junction x="104.14" y="71.12"/>
<pinref part="J1" gate="G$1" pin="8"/>
<pinref part="J1" gate="G$1" pin="6"/>
<wire x1="124.46" y1="73.66" x2="124.46" y2="71.12" width="0.1524" layer="91"/>
<wire x1="124.46" y1="68.58" x2="124.46" y2="71.12" width="0.1524" layer="91"/>
<junction x="124.46" y="71.12"/>
<pinref part="R3" gate="G$1" pin="P$1"/>
<wire x1="124.46" y1="73.66" x2="127" y2="73.66" width="0.1524" layer="91"/>
<junction x="124.46" y="73.66"/>
<pinref part="C3" gate="G$1" pin="P$1"/>
<wire x1="127" y1="73.66" x2="132.08" y2="73.66" width="0.1524" layer="91"/>
<wire x1="132.08" y1="73.66" x2="132.08" y2="71.12" width="0.1524" layer="91"/>
<junction x="127" y="73.66"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VDD18"/>
<pinref part="C4" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="XTAL1_(CLKIN)"/>
<pinref part="U1" gate="G$1" pin="OUTPUT"/>
<wire x1="208.28" y1="40.64" x2="208.28" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ACCESS" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="LED"/>
<pinref part="R1" gate="G$1" pin="P$2"/>
<wire x1="187.96" y1="10.16" x2="190.5" y2="10.16" width="0.1524" layer="91"/>
<label x="187.96" y="10.16" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPIO_24/_RMII_MDIO"/>
<wire x1="38.1" y1="-27.94" x2="35.56" y2="-27.94" width="0.1524" layer="91"/>
<label x="35.56" y="-27.94" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="P$1"/>
<pinref part="D1" gate="G$1" pin="ANODE"/>
<wire x1="177.8" y1="10.16" x2="177.8" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPIO_16/_RMII_RXD0/_DAC_16"/>
<pinref part="R5" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="P$1"/>
<pinref part="D2" gate="G$1" pin="ANODE"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
